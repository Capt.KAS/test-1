plugins {
    id("java-library")
}

group = "org.example.hsbctest"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    annotationProcessor("org.projectlombok:lombok:1.18.30")
    implementation("org.projectlombok:lombok:1.18.30")
    implementation("ch.qos.logback:logback-classic:1.4.14")


    testAnnotationProcessor("org.projectlombok:lombok:1.18.30")
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.projectlombok:lombok:1.18.30")
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.assertj:assertj-core:3.25.3")
    testImplementation("ch.qos.logback:logback-classic:1.4.14")

}

tasks.test {
    useJUnitPlatform()
}