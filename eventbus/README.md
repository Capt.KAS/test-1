
My first idea was to implement a solution inspired by Apache Kafka and the LMAX architecture.
It would have had circular buffers on either side of a middle-man thread whose job would have been to sleep until the next event
and then distribute it to the appropriate waiting consumers.

But then I figured it probably would have been impossible for me to do something as fast and reliable as LMAX,
at least not with the time I have available for this test.
So as I was experimenting with the first solution, I thought of the observer pattern I did for part one and realized that
instead of copying the events from buffer to buffers, I could just distribute references of consumers that are ready to process events
and let producers notify them.

Currently, I have a working proof of concept for the second solution.
It still needs a lot of optimizations but at least it looks like it works and I know what I need to do
