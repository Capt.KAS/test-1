package org.example.hsbctest.eventbus;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;

@Slf4j
@AllArgsConstructor
public class EventConsumerTask<T> implements Callable<T> {
    private final MyEventBus<T> eventBus;
    private final String name;
    private final String topic;
    private final String group;
    private final long sleepBefore;

    @Override
    public T call() {
        MyEventBusConsumer<T> consumer = eventBus.createConsumer(topic, group);
        try {
            Thread.sleep(sleepBefore);
            log.debug("{} starts waiting", name);
            T event = consumer.waitForNextEvent();
            log.debug("{} receives {}", name, event);
            return event;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
