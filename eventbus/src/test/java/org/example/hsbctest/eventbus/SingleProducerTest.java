package org.example.hsbctest.eventbus;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.assertj.core.api.SoftAssertions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.FutureTask;

@Slf4j
public class SingleProducerTest {
    static final int TEST_RANGE = 10000;
    static final int CONSUMER_COUNT = 6;
    private MyEventBus<Integer> eventBus;
    private SoftAssertions softly;

    @BeforeEach
    void before() {
        this.eventBus = new MyEventBus<>();
        this.softly = new SoftAssertions();
    }

    @Test
    void should_send_and_receive_in_order() throws InterruptedException {
        MyEventBusConsumer<Integer> consumer = eventBus.createConsumer("t1", "g1");
        for (int i = 0 ; i < TEST_RANGE ; i += 1) {
            new Thread(new MockEventBusProducer<>(eventBus, "writer" + i, 0, i, "t1")).start();
            Integer next = consumer.waitForNextEvent();
            softly.assertThat(next).isEqualTo(i);
        }
        softly.assertAll();
    }

    @Test
    void multiple_consumers() {
        List<Boolean> statusArr = new ArrayList<>(Collections.nCopies(CONSUMER_COUNT, false));
        List<FutureTask<Integer>> tasksArr = new ArrayList<>(Collections.nCopies(CONSUMER_COUNT, null));
        for (int i = 0 ; i < TEST_RANGE ; i += 1) {
            for (int j = 0 ; j < CONSUMER_COUNT ; j += 1) {
                if (!statusArr.get(j)) {
                    tasksArr.set(j, new FutureTask<>(new EventConsumerTask<>(eventBus, "reader" + j, "t1", "g1", 0)));
                    new Thread(tasksArr.get(j)).start();
                    statusArr.set(j, true);
                }
            }
            new Thread(new MockEventBusProducer<>(eventBus, "writer" + i, 0, i, "t1")).start();
            Integer next = -1;
            for (int j = 0 ; next.equals(-1) ; j += 1) {
                if (tasksArr.get(j).isDone()) {
                    next = tasksArr.get(j).resultNow();
                    statusArr.set(j, false);
                }
                if (j + 1 == CONSUMER_COUNT) {
                    j = -1;
                }
            }
            softly.assertThat(next).isEqualTo(i);
        }
        softly.assertAll();
    }

    private void consumersAndGroupsTest(String topic) {
        List<Boolean> statusArr = new ArrayList<>(Collections.nCopies(CONSUMER_COUNT, false));
        List<FutureTask<Integer>> tasksArr = new ArrayList<>(Collections.nCopies(CONSUMER_COUNT, null));
        for (int i = 0 ; i < TEST_RANGE ; i += 1) {
            for (int j = 0 ; j < CONSUMER_COUNT ; j += 1) {
                if (!statusArr.get(j)) {
                    tasksArr.set(j, new FutureTask<>(new EventConsumerTask<>(eventBus, "reader" + j, topic, "g" + j % 2, 0)));
                    new Thread(tasksArr.get(j)).start();
                    statusArr.set(j, true);
                }
            }
            new Thread(new MockEventBusProducer<>(eventBus, "writer" + i, 0, i, topic)).start();
            Integer nextGroup1 = -1;
            Integer nextGroup2 = -1;
            for (int j = 0 ; nextGroup1.equals(-1) || nextGroup2.equals(-1) ; j += 1) {
                if (tasksArr.get(j).isDone()) {
                    if (j % 2 == 0) {
                        nextGroup1 = tasksArr.get(j).resultNow();
                        log.debug("Done {}", nextGroup1);
                        statusArr.set(j, false);
                    } else {
                        log.debug("Done {}", nextGroup2);
                        nextGroup2 = tasksArr.get(j).resultNow();
                        statusArr.set(j, false);
                    }
                }
                if (j + 1 == CONSUMER_COUNT) {
                    j = -1;
                }
            }
            log.debug("next {}", nextGroup1);
            softly.assertThat(nextGroup1).isEqualTo(i);
            softly.assertThat(nextGroup2).isEqualTo(i);
        }
        softly.assertAll();
    }

    @Test
    void all_groups_receive_all_messages_in_order() {
        consumersAndGroupsTest("t1");
    }

    @Test
    void multiple_topics() {
        new Thread(() -> consumersAndGroupsTest("t1")).start();
        new Thread(() -> consumersAndGroupsTest("t2")).start();
        new Thread(() -> consumersAndGroupsTest("t3")).start();
    }

}
