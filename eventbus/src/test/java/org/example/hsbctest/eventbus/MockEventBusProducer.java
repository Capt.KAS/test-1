package org.example.hsbctest.eventbus;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class MockEventBusProducer<T> implements Runnable {
    private final MyEventBus<T> eventBus;
    private final String name;
    private final long sleepFor;
    private final T msg;
    private final String topic;

    @Override
    public void run() {
        try {
            Thread.sleep(sleepFor);
            log.debug("{} about to write {} to topic {}", name, msg, topic);
            eventBus.writeToTopic(topic, msg);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
