package org.example.hsbctest.eventbus;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class SingleThreadedEventBus<T extends Serializable> implements EventBus<T> {
    private final Set<Subscriber<T>> subscribers;

    public SingleThreadedEventBus() {
        subscribers = new HashSet<>();
    }

    @Override
    public void publishEvent(T o) {
        subscribers.forEach(s -> s.update(o));
    }

    @Override
    public void addSubscriber(Subscriber<T> subscriber) {
        subscribers.add(subscriber);
    }
}
