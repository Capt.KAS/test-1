package org.example.hsbctest.eventbus;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
public class MyEventBusConsumer<T> {
    private final static long BUSY = 0;
    private final static long READY_TO_RECEIVE = 1;
    private final static long RECEIVING = 2;
    private final static long READY_TO_PROCESS = 3;

    public AtomicLong status = new AtomicLong(BUSY);

    private CountDownLatch latch = new CountDownLatch(1);

    @Getter
    private final String groupId;

    private final EventQueue eventQueue = new EventQueue();

    protected MyEventBusConsumer(String groupId) {
        this.groupId = groupId;
    }

    private boolean checkStatusMatches(long toMatch) {
        return status.get() == toMatch;
    }

    public boolean isReadyToReceive() {
        return checkStatusMatches(READY_TO_RECEIVE);
    }

    public boolean isReadyToProcess() {
        return checkStatusMatches(READY_TO_PROCESS);
    }

    // By acquiring we go from any number of threads possibly modifying to only a single one
    public boolean tryAcquireForReceiving() {
        return status.compareAndSet(READY_TO_RECEIVE, RECEIVING);
    }

    public void event(T o) {
        log.debug("EVENT");
        eventQueue.add(o);
        this.status.set(READY_TO_PROCESS);
        latch.countDown();
    }

    private void initLatch() {
        this.status.compareAndSet(BUSY, READY_TO_RECEIVE);
    }

    // One consumer per thread
    public T waitForNextEvent() throws InterruptedException {
        initLatch();
        try {
            //boolean readyToProcess = false;
            while (!isReadyToProcess()) {
                log.debug("Going to sleep {}", latch.getCount());
                latch.await();
                log.debug("Woke up");
                initLatch();
            }
            return (T) eventQueue.get();
        } finally {
            //eventWrapper.event = null;
            if (eventQueue.readIndex() < eventQueue.writeIndex()) {
                this.status.set(READY_TO_PROCESS);
                latch = new CountDownLatch(0);
            } else {
                this.status.set(BUSY);
                latch = new CountDownLatch(1);
            }
        }
    }
}
