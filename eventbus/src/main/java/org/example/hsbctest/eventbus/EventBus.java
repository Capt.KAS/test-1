package org.example.hsbctest.eventbus;

import java.io.Serializable;

public interface EventBus<T extends Serializable> {
    // Feel free to replace Object with something more specific,
    // but be prepared to justify it
    void publishEvent(T o);
    // How would you denote the subscriber?
    void addSubscriber(Subscriber<T> subscriber);
    // Would you allow clients to filter the events they receive? How would the interface look like?
    //void addSubscriberForFilteredEvents();

    interface Subscriber<T> {
        void update(final T o);
    }
}
