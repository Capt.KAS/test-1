package org.example.hsbctest.eventbus;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicInteger;


@Slf4j
public class EventQueue {
    private static final int BUFFER_SIZE = 256;
    private final Object[] buffer = new Object[BUFFER_SIZE];
    private final AtomicInteger readIdx = new AtomicInteger(-1);
    private final AtomicInteger writeIdx = new AtomicInteger(0);

    public void add(Object o) {
        log.debug("ADD");
        readIdx.compareAndSet(-1, 0);
        buffer[writeIdx.getAndAccumulate(1, (a, b) -> (a + b) % BUFFER_SIZE)] = o;
    }

    public Object get() {
        log.debug("GET");
        return buffer[readIdx.getAndAccumulate(1, (a, b) -> (a + b) % BUFFER_SIZE)];
    }

    public int readIndex() {
        if (readIdx.get() == -1) {
            return Integer.MAX_VALUE;
        }
        return readIdx.get();
    }

    public int writeIndex() {
        return writeIdx.get();
    }
}
