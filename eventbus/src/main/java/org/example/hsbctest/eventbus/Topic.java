package org.example.hsbctest.eventbus;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;
import java.util.*;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
// package protected
class Topic<T> {
    private final String name;
    // TODO: Prettify this type
    private final Map<String, List<MyEventBusConsumer<T>>> consumersByGroups = new LinkedHashMap<>();

    // package protected constructor so that only the event bus creates them
    MyEventBusConsumer<T> createConsumer(String groupId) {
        // I am guessing no need to lock since there already are locks in EventBus methods and only EventBus should manipulate topics
        MyEventBusConsumer<T> nextConsumer = new MyEventBusConsumer<>(groupId);
        if (!consumersByGroups.containsKey(groupId)) {
            consumersByGroups.put(groupId, new LinkedList<>());
        }
        consumersByGroups.get(groupId).add(nextConsumer);
        return nextConsumer;
    }

    private void fallbackOnAnyConsumerOfGroup(String group, T o) throws InterruptedException {
        // TODO: Optimize this by scattering the messages to different consumers
        /*boolean sent = false;
        Iterator<MyEventBusConsumer<T>> it = consumersByGroups.get(group).iterator();
        while (!sent) {
            if (!it.hasNext()) {
                it = consumersByGroups.get(group).iterator();
            }
            sent = it.next().tryAcquireForReceiving();
        }*/
        consumersByGroups.get(group).getFirst().event(o); // TODO: Bufferize
    }

    /*private boolean tryToSendEventToGroup(List<MyEventBusConsumer<T>> consumersInGroup, T o) throws InterruptedException {
        for (MyEventBusConsumer<T> consumer : consumersInGroup) {
            if (consumer.isReadyToReceive() && consumer.tryAcquireForReceiving()) {
                consumer.event(o);
                //consumer.commit();
                return true;
            }
        }
        return false;
    }*/

    public void event(T o) throws InterruptedException {
        for (Map.Entry<String, List<MyEventBusConsumer<T>>> groupsInTopic : consumersByGroups.entrySet()) {
            boolean sentToGroup = false;
            for (MyEventBusConsumer<T> consumer : groupsInTopic.getValue()) {
                // If mode is keep all, use ready to receive or buffer in a queue
                // If mode is keep last, use ready to process in priority and replace their buffer placeholder
                // This is so we can be able to replace the last message sent before it is processed
                // Otherwise use ready to receive or fallbackOnAny
                if (consumer.isReadyToReceive() && consumer.tryAcquireForReceiving()) {
                    consumer.event(o);
                    sentToGroup = true;
                    break;
                }
            }
            if (!sentToGroup) {
                fallbackOnAnyConsumerOfGroup(groupsInTopic.getKey(), o);
            }
        }
    }
}
