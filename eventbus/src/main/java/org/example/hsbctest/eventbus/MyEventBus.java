package org.example.hsbctest.eventbus;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MyEventBus<T> {
    private final ReentrantReadWriteLock topicLock = new ReentrantReadWriteLock();
    private final Map<String, Topic<T>> topics = new HashMap<>();

    // We could separate the topic and group definition, either we make it user-friendly or debug-friendly
    // (which in itself is user-friendly, but only when issues arise)
    public MyEventBusConsumer<T> createConsumer(String topicId, String groupId) {
        try {
            // We could probably optimize but users should seldomly add and remove consumers during execution
            topicLock.writeLock().lock();
            if (!topics.containsKey(topicId)) {
                Topic<T> t = new Topic<>(topicId);
                topics.put(topicId, t);
            }
            return topics.get(topicId).createConsumer(groupId); // Watch out for no producers
        } finally {
            topicLock.writeLock().unlock();
        }
    }

    public void writeToTopic(String topicId, T o) throws InterruptedException {
        topicLock.readLock().lock();
        if (!topics.containsKey(topicId)) {
            throw new IllegalArgumentException("No topic " + topicId);
        }
        topics.get(topicId).event(o);
        topicLock.readLock().unlock();
    }
}
