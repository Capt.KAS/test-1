package org.example.hsbctest.eventbus;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class Main {
    /*public static class EventBusClient implements SingleThreadedEventBus.Subscriber<String> {
        private final String nameTag;
        private final SingleThreadedEventBus<String> eventBus;

        public EventBusClient(final String nameTag, final SingleThreadedEventBus<String> eventBus) {
            this.nameTag = nameTag;
            this.eventBus = eventBus;
            eventBus.addSubscriber(this);
        }

        public void writeMsg(final String message) {
            System.out.println(nameTag +  " - Write message : " + message);
            eventBus.publishEvent(message);
        }

        @Override
        public void update(final String s) {
            System.out.println(nameTag + " - Read message : " + s);
        }
    }

    public static void main(String[] args) {
        SingleThreadedEventBus<String> steb = new SingleThreadedEventBus<>();
        EventBusClient client1 = new EventBusClient("Client 1", steb);
        EventBusClient client2 = new EventBusClient("Client 2", steb);
        client1.writeMsg("ABC");
        client2.writeMsg("DEF");
        client1.writeMsg("GHI");
    }*/

    @Slf4j
    @AllArgsConstructor
    public static class EventBusWriter implements Runnable {
        private final MyEventBus<String> eventBus;
        private final String name;
        private final long sleepFor;
        private final long repeat;
        private final String msg;
        private final String topic;
        @Override
        public void run() {
            try {
                for (int i = 0 ; i < repeat ; i += 1) {
                    Thread.sleep(sleepFor);
                    log.info("{} about to write {} to topic {}", name, msg + i, topic);
                    eventBus.writeToTopic(topic, msg);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Slf4j
    @AllArgsConstructor
    public static class EventBusReader implements Runnable {
        private final MyEventBusConsumer<String> consumer;
        private final String name;
        private final long sleepBefore;
        private final long sleepAfter;
        @Override
        public void run() {
            try {
                Thread.sleep(sleepBefore);
                String event = null;
                for (int i = 0 ; i < 100 ; i += 1) {
                    log.debug("{} starts waiting", name);
                    event = consumer.waitForNextEvent();
                    log.debug("{} receives '{}'", name, event);
                    Thread.sleep(sleepAfter);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        // Noticed that when using multiple consumers in the same group, order might get lost ?
        // Unlikely but need tests to better monitor this
        // #NotABugButAFeature
        MyEventBus<String> eventBus = new MyEventBus<>();
        List<Thread> threads = new ArrayList<>(6);
        for (int i = 0 ; i < 2 ; i += 1) {
            EventBusReader reader = new EventBusReader(eventBus.createConsumer("t1", "group1"), "readerA" + i, 0, 2);
            threads.add(new Thread(reader));
        }
        /*for (int i = 0 ; i < 1 ; i += 1) {
            EventBusReader reader = new EventBusReader(eventBus.createConsumer("t1", "group2"), "readerB" + i, 0, 1);
            threads.add(new Thread(reader));
        }*/
                /*
               EventBusWriter writer1 = new EventBusWriter(eventBus, "writer1", 0L, 1, "FI", "t1");
        EventBusWriter writer2 = new EventBusWriter(eventBus, "writer2", 20L, 1,"FA", "t1");
        EventBusWriter writer3 = new EventBusWriter(eventBus, "writer3", 440L, 1, "FO", "t1");
        EventBusReader reader1 = new EventBusReader(eventBus.createConsumer("t1", "group1"), "reader1", 0, 0);
        EventBusReader reader2 = new EventBusReader(eventBus.createConsumer("t1", "group1"), "reader2", 0, 0);
        EventBusReader reader3 = new EventBusReader(eventBus.createConsumer("t1", "group2"), "reader3", 0, 0);
        threads = Arrays.asList(
                new Thread(writer1),
                new Thread(reader1),
                new Thread(reader2),
                new Thread(writer2),
                new Thread(reader3),
                new Thread(writer3)
        );*/

        threads.forEach(Thread::start);
        for (int i = 0 ; i < 10 ; i += 1) {
            log.info("Write {}", i);
            //Thread.sleep(i % 2 == 1 ? 100 : 0);
            eventBus.writeToTopic("t1", "" + i);
        }
    }
}