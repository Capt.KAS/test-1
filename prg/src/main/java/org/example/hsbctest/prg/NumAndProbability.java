package org.example.hsbctest.prg;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// Added setters and noArgs construct to facilitate loading from config in tests
// Losing the final aspect of the inner members but should not impact performance
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NumAndProbability {
    private int number;
    private float probabilityOfSample;
}
