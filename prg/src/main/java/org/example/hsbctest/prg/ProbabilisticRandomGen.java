package org.example.hsbctest.prg;

public interface ProbabilisticRandomGen {
    public int nextFromSample();
}
