package org.example.hsbctest.prg;

import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/*
    Notes from reading the exercise :
    Going for a kind of pachinko logic, idea would be to imagine a straight line separated in segments and see in which segment the puck drops
    Each segment's size represent their probability of being picked
    Ideally this "line" is abstractly represented in memory
    Maybe only keep the segment limits in memory
    Ex: A 0.5 B 0.3 C 0.2
    If we use a 1000m (is there a better number for this ? Bigger == More precise, more possible outcomes ?)
    We get the following list of ranges, A : 1 -> 500, B : 501 -> 800, C : 801 -> 1000 (Possibly adapt to 0 -> 999)
    We pick a number between 1 and 1000 and check where it belongs
    Parse list to see which range we fall into ? Not great if the list of possible numbers is huge, "spatial" data structure ? Will check
 */
public class MyProbabilisticRandomGen implements ProbabilisticRandomGen {
    private static final double FIELD_SIZE = Double.MAX_VALUE;

    // Following stores numberSegments by their highs so that we can find the ceiling entry of our drawn number in log(n)
    private final TreeMap<Double, Integer> numberSegmentsRTree;

    public MyProbabilisticRandomGen(final List<NumAndProbability> numAndProbabilities) {
        var wrp = new Object() { double index = 0; };
        numberSegmentsRTree = numAndProbabilities.stream()
                .collect(Collectors.toMap(
                        nap -> wrp.index += (FIELD_SIZE * nap.getProbabilityOfSample()),
                        NumAndProbability::getNumber,
                        (left, right) -> right, // Not called since numbers to draw should be unique here
                        TreeMap::new
                ));
    }

    @Override
    public final int nextFromSample() {
        double randomNumber = ThreadLocalRandom.current().nextDouble(FIELD_SIZE);
        return numberSegmentsRTree.ceilingEntry(randomNumber).getValue();
    }
}
