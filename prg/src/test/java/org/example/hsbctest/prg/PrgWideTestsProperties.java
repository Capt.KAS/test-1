package org.example.hsbctest.prg;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "wide-test")
@Data
public class PrgWideTestsProperties {
    private List<NumAndProbability> numbersAndProbabilities;
    private List<WideTestProperties> testProperties;

    @Data
    static class WideTestProperties {
        private String testName;
        private int nbTests;
        private double minPrecision;
    }
}
