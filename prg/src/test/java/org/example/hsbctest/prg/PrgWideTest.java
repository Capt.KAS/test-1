package org.example.hsbctest.prg;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ContextConfiguration(classes = PrgWideTestsProperties.class)
@Slf4j
public class PrgWideTest {
    @Autowired
    private PrgWideTestsProperties properties;
    private MyProbabilisticRandomGen prg;

    @BeforeEach
    void setup() {
        this.prg = new MyProbabilisticRandomGen(properties.getNumbersAndProbabilities());
    }

    private void verifyResults(final PrgWideTestsProperties.WideTestProperties testProperties,
                               final Map<Integer, Long> numbersAndNbPicked,
                               final Map<Integer, Float> numAndProbabilities) {
        assertTrue(numbersAndNbPicked.entrySet().stream().allMatch(entry -> {
            float expectedProbability = numAndProbabilities.getOrDefault(entry.getKey(), 0f);
            float measuredProbability = ((float) entry.getValue() / testProperties.getNbTests());
            float measuredPrecision = measuredProbability > expectedProbability ?
                    expectedProbability / measuredProbability : measuredProbability / expectedProbability;
            log.info("[WIDE TEST] {} - For number {} expected {} got {} - {}% precision, min is {}",
                    testProperties.getTestName(),
                    entry.getKey(),
                    expectedProbability,
                    measuredProbability,
                    measuredPrecision,
                    testProperties.getMinPrecision()
            );
            return measuredPrecision >= testProperties.getMinPrecision();
        }));
    }

    private Map<Integer, Long> runTest(final PrgWideTestsProperties.WideTestProperties testProperties) {
        return IntStream.range(0, testProperties.getNbTests()).parallel()
                .mapToObj(ignored -> prg.nextFromSample())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    private DynamicTest createWideTest(final PrgWideTestsProperties.WideTestProperties testProperties) {
        return DynamicTest.dynamicTest(testProperties.getTestName(), () -> {
            Map<Integer, Long> numbersAndNbPicked = runTest(testProperties);
            Map<Integer, Float> numAndProbabilitiesMap = properties.getNumbersAndProbabilities().stream().collect(
                    Collectors.toMap(
                            NumAndProbability::getNumber,
                            NumAndProbability::getProbabilityOfSample
                    )
            );
            verifyResults(testProperties, numbersAndNbPicked, numAndProbabilitiesMap);
        });
    }

    @TestFactory
    Stream<DynamicTest> loadDynamicTestsFromConfig() {
        return properties.getTestProperties().stream().map(this::createWideTest);
    }

}
