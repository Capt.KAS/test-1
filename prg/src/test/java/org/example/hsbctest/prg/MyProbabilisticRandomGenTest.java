package org.example.hsbctest.prg;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;

public class MyProbabilisticRandomGenTest {

    private static List<NumAndProbability> createSample() {
        return Arrays.asList(
                new NumAndProbability(1, 0.5f),
                new NumAndProbability(2, 0.3f),
                new NumAndProbability(3, 0.2f)
        );
    }

    @Test
    void shouldRetrieveNumberFromSample() {
        List<NumAndProbability> sample = createSample();
        MyProbabilisticRandomGen prg = new MyProbabilisticRandomGen(sample);
        int randomNumber = prg.nextFromSample();
        assertTrue(sample.stream().anyMatch(nap -> nap.getNumber() == randomNumber), () -> "Unknown outcome picked");
    }
}
