# Probabilistic Number Generator

## Algorithm

Constructor : O(n.log(n))
* Get a list of NumAndProbability in constructor
* Insert them progressively into tree map
* Use increments of current += Double.MAX * probability as map key

When called : O(log(n))
* ThreadLocalRandom is used between 0 and Double.MAX to obtain a random number
* Use treemap.ceiling to find the number in which "zone" we randomly land in
* The treemap serves as a simple kind of RTree in which zones do not overlap

## Tests

In addition to a basic unit test we featured a "wide" test.

This test will use a provided list of NumAndProbability,
draw a set amount of times and compare the obtained repartition with the one provided
up to a certain precision. 

It is fully configurable from yml using spring-test

There are two test sets, the first has half a dozen numbers with equivalent probabilities,
the second has two numbers, one at 99% and the other at 1%.
Draws are made up to a billion times in both set.

When doing tests with the first dataset I noticed the lower probabilities tend to deviate more,
this may be aggravated by the use of a double for the random number chosen,
the algorithm could easily be adapted to use long.
