# HSBC Test

## Antoine CASSE 2024

### Config Used :

- OpenJDK 21.0.2
- Gradle 8.5

### Dependencies :

|         Package          |           Name           | Version | Main scope |         Usage          |
|:------------------------:|:------------------------:|:-------:|:----------:|:----------------------:|
|    org.projectlombok     |          lombok          | 1.18.30 |    Yes     |         Lombok         |
|    org.junit.jupiter     |      junit-jupiter       | 5.10.2  |     No     |       Unit tests       |
| org.springframework.boot | spring-boot-starter-test |  3.2.3  |     No     | Test suite integration |

### Content :

[Probabilistic Number Generator](/prg/README.md)

[Event Bus](/eventbus/README.md)

